#pragma once

template<typename T>

class HashItem

{

public:

	HashItem() {}

	~HashItem() {}

	int GetKey() { return mKey; }

	void SetKey(int k) { mKey = k; }

	T GetObject() { return mObject; }

	void SetObj(T obj) { mObject = obj; }

	bool operator==(const HashItem& other)

	{
		if (other.mKey == mKey)
		{
			return true;
		}
		else
			return false;
	}

	void operator=(HashItem& other)

	{
		return mKey = other.mKey && mObject = other.mObject;
	}

private:
	int mKey;
	T mObject;

};