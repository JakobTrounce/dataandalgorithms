#ifndef INCLUDED_STACK_H
#define INCLUDED_STACK_H

template<typename T>
class Stack
{
public:
	struct Node
	{
		T data;
		Node *next;
	};
	void Push(T value)
	{
		Node* newNode;
		newNode = new Node;
		newNode->next = head;
		head = newNode;
		head->data = value;
		++count;
	}

	void POP()
	{
		if (count > 0)
		{
			Node* oldhead = head;
			head = head->next;
			--count;
		}
		delete oldhead;
	}

	T Peek()
	{
		if (count > 0)
		{
			return head->data;
		}
	}

	bool IsEmpty()
	{
		if (count > 0)
		{
			return false;
		}
		else
			return true;
	}

private:
	Node * head = nullptr;
	int count = 0;

};


#endif
