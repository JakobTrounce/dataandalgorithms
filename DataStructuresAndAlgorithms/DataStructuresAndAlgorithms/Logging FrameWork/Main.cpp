#include <thread>
#include "DataTable.h"
#include <chrono>
#include <ctime>
using std::thread;


int main()
{
	DataTable table;
	std::string time("09/08/18 ");
	std::string id("1730027 ");
	std::string action("4526 ");
	std::string parameter("attacking,running ");
	for (int i = 0; i < 10000; ++i)
	{
		std::chrono::system_clock::time_point p = std::chrono::system_clock::now();
		std::time_t t = std::chrono::system_clock::to_time_t(p);
		time = std::ctime(&t);
		thread store(&DataTable::storeData, &table,time, id, action, parameter);
		thread write(&DataTable::WriteToDisk, &table);
		store.join();
		write.join();
	}
	
}