#ifndef INCLUDED_STACKEDQUEUE_H
#define INCLUDED_STACKEDQUEUE_H

#include <stack>
template<typename T>
class StackedQueue
{
public:
	void Enqueue(T val)
	{
		if (!mDequeueStack.empty())
		{
			for (int i = 0; i < mDequeueStack.size(); ++i)
			{
				mEnqueueStack.push(mDequeueStack.top());
				mDequeueStack.pop();
			}
		}

		mEnqueueStack.push(val);
		count++;
	}
	void Dequeue()
	{
		if (!mDequeueStack.empty())
		{
			for (int i = 0; i < mDequeueStack.size(); ++i)
			{
				mDequeueStack.push(mEnqueueStack.top());
				mEnqueueStack.pop();
			}
		}
		count++;
	}
	T Peek()
	{
		if (mEnqueueStack.empty())
		{
			return mDequeueStack.top();
		}
		else
		{
			return mEnqueueStack.top();
		}
	}
private:
	std::stack<T> mEnqueueStack;
	std::stack<T> mDequeueStack;
	int count = 0;
};

#endif