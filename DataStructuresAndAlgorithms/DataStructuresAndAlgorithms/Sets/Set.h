#pragma once

#include "DynamicArray.h"

template <class T>
class Set<T>

{

private:

	DynamicArray<T> _items;

public :

	void Add(T entity)

	{
		_items.Add(entity);
	}

	void AddRange(DynamicArray<T> entities)

	{
		for (int i = 0; !entities.IsEmpty();)
		{
			_items.Add(entities[i]);
			entities.Remove(entities[i]);
		}
	}

	void AddSkipDuplicate(T entity)

	{
		for (int i = 0; i < _items.size())
		{
			if (_items[i] == entity)
			{
				return;
			}
		}
		_items.Add(entity);
	}

	void AddRangeSkipDuplicates(Set<T> items)

	{

		for (int i = 0; !items.IsEmpty();)

		{
			
			if (_items[i] == items[i])
			{

				continue;
			}
			else
			{
				_items.Add(items[i]);
			}

		}

	}
	DynamicArray<T> GetItems()
	{
		return _items;
	}

	// add everything from current set

	// add only items from the otherSet that are not in the

	// current set

	Set<T> Union(Set<T> otherSet)

	{

		Set<T> result = new Set<T>(_items);

		Result.AddRangeSkipDuplicates(other.GetItems());

		return result;

	}

	bool Contains(T item)

	{
		for (int i = 0; i < _items.size(); ++i)
		{
			if (_items[i] == item)
			{
				return true;
			}
		}
		return false;
	}

	int CompareTo(T other)

	{
		if (_items[0] < other)
		{
			return -1;
		}
		else if (_items[0] > other)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

}