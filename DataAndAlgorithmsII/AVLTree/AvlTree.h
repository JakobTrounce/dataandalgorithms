#ifndef INCLUDED_AVLTREE_H
#define INCLUDED_AVLTREE_H
#include "AVLTreeNode.h"
template <typename T>
class AvlTree
{
public:
	AvlTree()
	{
		AvlTreeNode<T>* node;
			mRoot = new AvlTreeNode<T>(0, node, *this);
	}
	void AddTo(AvlTreeNode<T>node, T data)
	{
		if (node.CompareTo(data) < 0)
		{
			if (node.left == nullptr)
			{
				node.left = new AvlTreeNode<T>(data, node, *this);
			}
			else
			{
				AddTo(*(node.left), data);
			}
		}
		else if (node.CompareTo(data) >= node.data)
		{
			if (node.right == nullptr)
			{
				node.right = new AvlTreeNode<T>(data, node, *this);
			}
			else
			{
				AddTo(*(node.right), data);
			}
		}
		node.mBalanceFactor = node.GetHeight(node.right) - node.GetHeight(node.left);
		node.Balance();
	}

	bool Contains(T data)
	{
		return Find(data != nullptr);
	}
	bool Remove(T data)
	{
		AvlTreeNode<T> current = Find(data);
		if (current == nullptr)
		{
			return false;
		}
		AvlTreeNode<T> treeNodeToBalance = current.parent;
		//case1
		if (current.Right == nullptr)
		{
			if (current.parent == nullptr)
			{
				mRoot = current.left;
				if (mRoot != nullptr)
				{
					mRoot.parent = nullptr;
				}
			}
			else
			{
				int result = current.parent.CompareTo(current.data);
				if (result > 0)
				{
					current.parent.left = current.left;
				}
				else if(result < 0)
				{
					current.parent.right = current.left;
				}
			}
		}
		//case2: if currents right child has no left chuld, then currents right child replaces curremt
		else if (current.Right.left == nullptr)
		{
			current.Right.left = current.left;
			if (current.parent == nullptr)
			{
				mRoot = current.Right;
				if (mRoot != nullptr)
				{
					mRoot.parent = nullptr;
				}
			}
			else
			{
				int result = current.parent.CompareTo(current.data);
				if (result > 0)
				{
					current.parent.left = current.Right;
				}
				else if (result < 0)
				{
					current.parent.right = current.right;
				}
			}
		}
		//case3: when current's right child has a left child, replace current with current's right child's left most child
		else
		{
			AvlTreeNode<T> leftMost = current.right.left;
			while (leftMost.left != nullptr)
			{
				leftMost = leftMost.left;
			}
			leftMost.parent.left = leftMost.right;
			leftMost.left = current.left;
			leftMost.right = current.right;

			if (current.parent == nullptr)
			{
				mRoot = leftMost;
				if (mRoot != nullptr)
				{
					mRoot.parent = nullptr;
				}
			}
			else
			{
				int result = current.parent.CompareTo(current.data);

				if (result > 0)
				{
					current.parent.left = leftMost;
				}
				else if (result < 0)
				{
					current.parent.right = leftMost;
				}
			}
		}
		if (treeNodeToBalance != nullptr)
		{
			treeNodeToBalance.Balance();
		}
		else
		{
			if (mRoot != nullptr)
			{
				mRoot.Balance();
			}
		}
		return true;
	}
	void InOrderTraversal(AvlTreeNode<T>* current)
	{
		if (current == nullptr)
		{
			return;
		}
		InOrderTraversal((current->left));
		Process(current->data);
		InOrderTraversal(current->right);
	}

	void PreOrderTraversal(AvlTreeNode<T> current)
	{
		if (current == nullptr)
		{
			return;
		}
		Process(current.data);
		PreOrderTraversal(current.left);
		PreOrderTraversal(current.right);
	}
	void PostOrderTraversal(AvlTreeNode<T> current)
	{
		if (current == nullptr)
		{
			return;
		}
		PostOrderTraversal(current.left);
		if (display)
		{
			std::cout << "/";
		}
		PostOrderTraversal(current.right);
		if (display)
		{
			std::cout << "\\" << std::endl;
		}
		Process(current.data);
	}
	AvlTreeNode<T> const GetRoot() const { return mRoot; }
	T Find(T data)
	{
		return Find(data, mRoot);
	}

	void DisplayTree()
	{
		display = true;
		InOrderTraversal(&mRoot);
		display = false;
	}
	bool display{ false };
	AvlTreeNode<T> mRoot;
	private:
		void Process(T data)
		{
			std::cout << data << std::endl;
		}
		T Find(T data, const AvlTreeNode<T>& current)
		{
			if (data < current.data && current.left != nullptr)
			{
				Find(data, *(current.left));
			}
			else if(data > current.data && current.right != nullptr)
			{
				Find(data, *(current.right));
			}
			if (data == current.data)
			{
				return data;
			}
			else
			{
				return nullptr;
			}
		}
	

};



#endif