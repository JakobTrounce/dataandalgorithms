#include "DataTable.h"

void DataTable::storeData(std::string date, std::string userId, std::string actionId, std::string actionParameters)
{
	playerData player
	{
		date,
		userId,
		actionId,
		actionParameters
	};
	stored.push(player);
	if (stored.size() >= 1)
	{
		mtx.lock();
		toWrite.push(stored.back());
		mtx.unlock();
	}
}

void DataTable::WriteToDisk()
{
	playerData data;
	if (toWrite.empty())
	{
		
	}
	else
	{
		storage.open("playerData.txt", std::ios::app);
		mtx.lock();
		data = toWrite.back();
		toWrite.pop();
		mtx.unlock();
		storage << "Date: " << data.date;
		storage << "UserId: " << data.userId;
		storage << "ActionId: " << data.actionId;
		storage << "ActionParameters: " << data.actionParameters << std::endl;
		storage.close();
	}
}
