#ifndef INCLUDED_AVLTREENODE_H
#define INCLUDED_AVLTREENODE_H
#include "TreeState.h"
#include <iostream>
#include <algorithm>
template <typename T>
class AvlTree;
template <typename T>
 class AvlTreeNode
{
public:
	AvlTreeNode(T data, AvlTreeNode<T> parent, AvlTree<T> tree)
	{

	}
	AvlTreeNode()
	{

	}

	 AvlTreeNode<T> *parent;
	 AvlTreeNode<T> *right;
	 AvlTreeNode<T> *left;
	 T data;
	 
	 int GetHeight(AvlTreeNode<T>*node)
	 {
		 if (node != nullptr)
		 {
			 return 1 + std::max(GetHeight(node->left), GetHeight(node->right));
		 }
		 return 0;
	 }
	 int CompareTo(T other)
	 {
		 if (this->data > other)
		 {
			 return 1;
		 }
		 if (this->data < other)
		 {
			 return -1;
		 }
		 if (this->data == data)
		 {
			 return 0;
		 }
	 }
	 
	 TreeState GetState()
	 {
		 if (GetHeight(left) - GetHeight(right) > 1)
		 {
			 return TreeState::LeftHeavy;
		 }
		 else if (GetHeight(left) - GetHeight(right) < 1)
		 {
			 return TreeState::RightHeavy;
		 }
		 else
		 {
			 return TreeState::Balanced;
		 }
	 }
	 void ReplaceRoot(AvlTreeNode<T>* newRoot)
	 {
		 if (parent != nullptr)
		 {
			 if (parent->left == this)
			 {
				 this->parent->left = newRoot;
			 }
			 else if (parent->right == this)
			 {
				 this->parent->right = newRoot;
			 }
		 }
		 else
		 {
			 mTree->mRoot = *newRoot;
		 }
		 newRoot->parent = this->parent;
		 this->parent = newRoot;
	 }

	 void LeftRotation()
	 {
		 AvlTreeNode<T>* newRoot = right;
		 ReplaceRoot(newRoot);
		 right = newRoot->left;
		 newRoot->left = this;
	 }

	 void RightRotation()
	 {
		 AvlTreeNode<T> *newRoot = left;
		 ReplaceRoot(newRoot);
		 left = newRoot->right;
		 newRoot->right = this;
	 }

	 void RightLeftRotation()
	 {
		 left->LeftRotation();
		 RightRotation();
	 }

	 void LeftRightRotation()
	 {
		 right->RightRotation();
		 LeftRotation();
	 }
	 void Balance()
	 {
		 if (GetState() == TreeState::RightHeavy)
		 {
			 if (right != nullptr && right->mBalanceFactor < 0)
			 {
				 LeftRightRotation();
			 }
			 else
			 {
				 LeftRotation();
			 }
		 }
		 else if (GetState() == TreeState::LeftHeavy)
		 {
			 if (left != nullptr && left->mBalanceFactor > 0)
			 {
				 RightLeftRotation();
			 }
			 else
			 {
				 RightRotation();
			 }
		 }
		 else if (GetState() == TreeState::Balanced)
		 {

		 }
	 }
	 int mBalanceFactor = GetHeight(right) - GetHeight(left);
private:
	
	AvlTree<T> *mTree;
};















#endif