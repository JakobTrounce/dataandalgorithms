#include "DynamicArray.h"
#include <iostream>


void main()
{
	DynamicArray<int> intarray1(2, 2);


	intarray1.Add(3);
	intarray1.Add(2);
	intarray1.Add(1);
	intarray1.Add(9);
	intarray1.Add(20);
	intarray1.Add(5);
	
	intarray1.Sort();

	for (int i = 0; i < 10; ++i)
	{
		std::cout << intarray1[i] << std::endl;;
	}


	system("pause");
}