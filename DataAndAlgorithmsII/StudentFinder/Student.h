#pragma once
#include <string>
#include "Tree.h"
class StudentFiling
{

public:
	struct Student
	{
		std::string firstName;
		std::string lastName;
		std::string email;
		int studentId;
	};
	void AddStudent(int id, std::string firstName, std::string lastName, std::string Email);
	Student FindStudent(int id);
	Student FindStudent(std::string firstName);
	Student FindStudent(std::string lastName);
	void RemoveStudent(int id);
	void UpdateStudent();
	void RetrieveAllStudents();
private:
	Tree<Student> students;
	Tree<std::string> names;
	Tree<std::string> emails;
	Tree<int> ids;
};