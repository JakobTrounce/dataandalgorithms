#include "PostFixCalc.h"


int PostFixCalc::EvaluateExpression(std::string exp)
{
	
	for (int i = 0; i < exp.length(); ++i)
	{
		if (exp[i] == ' ' || exp[i] == ',')
		{
			continue;
		}
		else if (IsOperator(exp[i]))
		{
			int num1 = expression.Peek();
			expression.Pop();
			int num2 = expression.Peek();
			expression.Pop();

			int result = Calculate(exp[i], num1, num2);

			expression.Push(result);
		}
		else if (IsNumeric(exp[i]))
		{

			int num = 0;
			while (i < exp.length() && IsNumeric(exp[i]))
			{
				num = (num * 10) + (exp[i] - '0');
				++i;
			}
			--i;
			expression.Push(num);
		}
	}
	return expression.Peek();

}

bool PostFixCalc::IsNumeric(char val)
{
	if (val >= '0' && val <= '9')
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool PostFixCalc::IsOperator(char val)
{
	if (val == '+' || val == '-' || val == '*' || val == '/')
	{
		return true;
	}
	else
	{
		return false;
	}
}

int PostFixCalc::Calculate(char operation, int num1, int num2)
{
	if (operation == '+')
	{
		return num1 + num2;
	}
	else if (operation == '-')
	{
		return num1 - num2;
	}
	else if (operation == '/')
	{
		return num1 / num2;
	}
	else if (operation == '*')
	{
		return num1 * num2;
	}
	else return num1;
}