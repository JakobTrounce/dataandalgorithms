#ifndef INCLUDED_GRAPH_H
#define INCLUDED_GRAPH_H
#include <vector>
#include "Vertex.h"
template <class T>
class Graph
{
public:
	Graph(int numVertices)
		:
		mMaxVertices(numVertices),
		mAdjMatrix(nullptr)
	{
		// allocate a 2 dimmensional array
	}
	void AttachEdge(int index1, int index2)
	{
		//m_adjMatrix[index1][index2] = 1
		// m_adjMatrix[index2][index1] = 1
	}
	void AttachDirectedEdge(int index1, int index2)
	{
		//m_adjMatrix[index1][index2] = 1
	}
private:
	int mMaxVertices;
	char *mAdjMatrix;
	// use char because char holds less memory
	// *1 byte*
	std::vector<Vertex<T>>mVertices[mMaxVertices];
};





#endif