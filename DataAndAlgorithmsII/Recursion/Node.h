#pragma once


/*
	add
	if > go right if < go left
	if no child then  insert
*/

template <typename T>
	struct Node
	{
		T mData;
		Node* leftNode;
		Node* rightNode;
	};

