#include "StackedQueue.h"
#include <assert.h>
#include <iostream>

void StackedQueueEnqueueHappyPath()
{
	StackedQueue<int> nums;
	nums.Enqueue(1);
	nums.Enqueue(2);
	nums.Enqueue(3);
	assert(nums.Peek() == 3, "[StackedQueue], the stored number is wrong");
}

void StackedQueueDequeueHappyPath()
{
	StackedQueue<int> nums;

	nums.Enqueue(1);
	nums.Enqueue(2);
	nums.Enqueue(3);
	nums.Dequeue();
	nums.Dequeue();
	assert(nums.Peek() == 3, "[StackedQueue], The Stored Number is wrong");
}

int main()
{
	StackedQueueEnqueueHappyPath();
	std::cout << std::endl;
	StackedQueueDequeueHappyPath();
	std::cout << std::endl;

	system("pause");
}