#pragma once
#include "DynamicArray.h"
#include <iostream>
template<typename T>
class ArrayStack
{
public:

	ArrayStack(int sz, int growth)
		:
		size(sz),
		grow(growth)
	{
	}
	void Push(T val)
	{
		stack.Add(val);
		count++;
		head = count;
	}

	void Pop()
	{
		try 
		{
			if (head < 0)
			{
				throw DynamicArrayException("Stack Empty");
			}
			stack.Remove(head);
			count--;
			head = count;
		}
		catch(const DynamicArrayException& e)
		{
			cerr << e.what();
		}
	}

	T Peek()
	{
		if (head > 0)
		{
			return stack[head];
		}
	}

	bool IsEmpty()
	{
		if (head < 0)
		{
			return true;
		}
		else
			return false;
	}

	void Clear()
	{
		stack.Clear();
	}
private:
	int count = -1;
	int head;
	int grow;
	DynamicArray<T> stack;
	int size;
};