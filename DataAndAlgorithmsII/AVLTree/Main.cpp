#include "AvlTree.h"

int main()
{
	AvlTree<int> tree;
	
	tree.AddTo(tree.GetRoot(), 10);
	tree.DisplayTree();
	
	return 0;
}