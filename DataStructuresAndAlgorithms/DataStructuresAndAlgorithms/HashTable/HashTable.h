#pragma once
#include <string>
#include "DynamicArray.h"
using std::string;
template<typename T>

class HashTable

{

public:

	HashTable() {}

	~HashTable() {}

public:

	bool Insert(string key, T &obj)

	{
		int index = HashFunction(key) % GetSize();
		table[index] = obj;

	}

	void Delete(string key)

	{
		int index = HashFunction(key) % GetSize();

		table.Remove(index);


	}

	bool Find(string key, T *obj)

	{
		int index = HashFunction(key) % GetSize();
		if (table[index] == obj)
		{
			return true;
		}
		else
			return false;
	}

public:

	int HashFunctionOne(std::string &str)

	{
		int hash = 0;
		for (int i = 0; i < str.length(); ++i)
		{
			hash += (int)str[i];
		}

		
		return hash;

	}

	int HashFunctionTwo(std::string &str)

	{
		int val = 0;
		for (int i = 0; i < str.length(); ++i)
		{
			val = (int)str[i];
			hash = (hash * 256 + val);
		}

		return hash;

	}

	int GetSize()

	{
		return table.GetSize();
	}

	int GetTotalItems()

	{
		return table.GetItems();
	}

private:
	DynamicArray<T> table;
};