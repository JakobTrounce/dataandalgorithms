
#include <assert.h>
#include <iostream>

float GetAverage(int iArray[], int size)
{
	float sum = 0.0f;

	for (int i = 0; i < size; ++i)
	{
		sum += iArray[i];
	}

	return sum / size;

}

void UnitTest()
{
	int numarray[5] = { 1,2,3,5,6 };
	assert(GetAverage(numarray, 5) >= 3.4);
}



int main()
{
	UnitTest();

	system("pause");
}