#ifndef INCLUDED_VERTEX_H
#define INCLUDED_VERTEX_H

template <class T>
class Vertex
{
public:
	Vertex(T node)
		:
		mNode(node)
	{
		
	}
	T GetNode()
	{
		return mNode;
	}
private:
	T mNode;
};





#endif
