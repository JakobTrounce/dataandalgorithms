#include "tree.h"
#include <assert.h>
#include <Windows.h>

void TreeAddHappyPath(Tree& tree);
void TreePrintPreOrderHappyPath(Tree& tree);
void TreeMinValueHappyPath(Tree& tree);
void TreeGetSuccessorHappyPath(Tree& tree);


int main()
{
	Tree tree(5);
	std::cout << "Running Tests..." << std::endl;
	Sleep(1000);
	 TreeAddHappyPath(tree);
	 Sleep(1000);
	 TreePrintPreOrderHappyPath(tree);
	 Sleep(1000);
	 TreeMinValueHappyPath(tree);
	 Sleep(1000);
	 TreeGetSuccessorHappyPath(tree);
	 Sleep(1000);
}


void TreeAddHappyPath(Tree& tree)
{

	tree.Add(4);
	tree.Add(2);
	tree.Add(8);
	tree.Add(6);
	tree.Add(11);
	tree.Add(7);
	assert(tree.Size() == 6);
	std::cout << "TreeAddHappyPath Test passed" << std::endl;
}

void TreePrintPreOrderHappyPath(Tree& tree)
{
	tree.PrintPreOrder(tree.GetRootNode());
	std::cout << "\nTreePrintPreOrderHappyPath Test passed" << std::endl;
}

void TreeMinValueHappyPath(Tree& tree)
{
	assert(tree.minValue(tree.GetRootNode()) == 2);

	std::cout << "TreeMinValueHappyPath Test passed" << std::endl;
}

void TreeGetSuccessorHappyPath(Tree& tree)
{
	assert(tree.GetSuccessor(2,tree.GetRootNode()) == 4);
	std::cout << "TreeGetSuccessorHappyPath Test passed" << std::endl;
}