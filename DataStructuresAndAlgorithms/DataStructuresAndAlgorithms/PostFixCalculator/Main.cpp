#include <iostream>
#include <string>
#include "PostFixCalc.h"
using std::cout;
using std::endl;
PostFixCalc calc;
int main()
{
	std::string expression;
	int result = 0;
	cout << "welcome to your post fix calculator!" << endl;

	do
	{
		cout << "Please use PostFix Notation! " << endl;
		cout << "Use # to Exit the program" << endl;
		cout << "Enter your expression: ";
		std::cin >> expression;
		
		result = calc.EvaluateExpression(expression);

		cout << " your result is: " << result;
	} while (expression != "#");

}
