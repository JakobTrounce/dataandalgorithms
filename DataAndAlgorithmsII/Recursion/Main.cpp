#include <iostream>
#include <math.h>
#include <cassert>
#include <vector>

// psuedo
/*
 if k = 1 return 0
 else
 return GetNthNath(nth-1) + 2
*/

int GetNthEvenNatRecursive(int nth)
{
	if (nth == 1)
	{
		return 0;
	}
	else
	{

		return GetNthEvenNatRecursive(nth - 1) + 2;
	}
}


int GetNthEvenNat(int nth)
{
	return pow(2, nth - 1);
}


void GetNthEvenNatRecursiveHappyPath()
{
	assert(GetNthEvenNatRecursive(3) == 4);
	std::cout << GetNthEvenNatRecursive(3) << std::endl;
}

void GetNthEvenNatHappyPath()
{
	assert(GetNthEvenNat(3) == 4);
	std::cout << GetNthEvenNat(3) << std::endl;
}

// fibonacci psuedo
/*
	number = 2 numbers before it
	
	k = 1 
	
	return k;
	else return fibonacci(k -1) + fibbonaci(k-2);
	

*/

int FibonacciRecursive(int nth,std::vector<int> &fibb)
{
	if (nth <= 2)
	{
		fibb[nth - 1] = 1;
		//fibb.push_back(1);
		return 1;
	}
	else
	{
		fibb[nth -1] = (FibonacciRecursive(nth - 1, fibb) + FibonacciRecursive(nth - 2, fibb));
		return FibonacciRecursive(nth - 1, fibb) + FibonacciRecursive(nth - 2, fibb);
	}
}


int main()
{
	std::vector<int> fibb;
	for (int i = 0; i < 4; ++i)
	{
		fibb.push_back(i);
	}
	GetNthEvenNatHappyPath();
	GetNthEvenNatRecursiveHappyPath();
	FibonacciRecursive(4,fibb);
	for (auto it = fibb.begin(); it != fibb.end(); ++it)
	{
		std::cout << *it;
	}
	std::cout << std::endl;
	system("pause");
}