#include "tree.h"


void Tree::InsertSortedNode(Node* node, Node& root)
{
	if (node->mData < root.mData && root.leftNode != nullptr)
	{
		InsertSortedNode(node, *(root.leftNode));
	}
	else if (node->mData > root.mData && root.rightNode != nullptr)
	{
		InsertSortedNode(node, *(root.rightNode));
	}
	else if(node->mData < root.mData)
	{
		root.leftNode = node;
	}
	else
	{
		root.rightNode = node;
	}
}
void Tree::Add(int value)
{
	if (!Contains(value,mRoot))
	{
		Node* temp = CreateNode(value);
		InsertSortedNode(temp, mRoot);
		++count;
	}
	else
	{

	}
}


Node* Tree::CreateNode(int data)
{
	Node* pNode = new Node();
	pNode->mData = data;
	pNode->leftNode = nullptr;
	pNode->rightNode = nullptr;

	return pNode;
}

void Tree::PrintPreOrder(const Node& node) const
{
	//root
	std::cout << node.mData;
	//left
	if (node.leftNode != nullptr)
	{
		PrintPreOrder(*(node.leftNode));
	}
	//right
	if (node.rightNode != nullptr)
	{
		PrintPreOrder(*(node.rightNode));
	}
}

int Tree::Size()
{
	return count;
}

int Tree::minValue(Node& root)
{
	minVal = root.mData;
	if (root.leftNode != nullptr)
	{
		minValue(*(root.leftNode));
	}
	else
	{
		minVal = root.mData;
		return minVal;
	}
	return minVal;
}

int Tree::GetSuccessor(int value,Node& root)
{
	if (Contains(value,root))
	{
		if (root.mData > value && root.leftNode != nullptr && root.leftNode->mData != value)
		{
			GetSuccessor(value, *(root.leftNode));
		}
		else if (root.mData < value && root.rightNode!= nullptr && root.rightNode->mData != value)
		{
			GetSuccessor(value, *(root.rightNode));
		}
		else if(root.mData > value)
		{
			return root.mData;
		}
		else
		{
			return -1;
		}
	}
}

bool Tree::Contains(int value,Node& root)
{
	if (value < root.mData && root.leftNode != nullptr)
	{
		Contains(value, *(root.leftNode));
	}
	else if (value > root.mData && root.rightNode != nullptr)
	{
		Contains(value, *(root.rightNode));
	}
	else if (value == root.mData)
	{
		return true;
	}
	else
	{
		return false;
	}
}