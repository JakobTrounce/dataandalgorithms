#pragma once
#include "Node.h"
#include <iostream>
class Tree
{
public:
	Tree()
	{
		{
			mRoot = Node();
			mRoot.mData = -1;
		}
	}
	Tree(int nodeValue)
	{
		mRoot = Node();
		mRoot.mData = nodeValue;
	}
	~Tree()
	{

	}
	void Add(int value);
	void PrintPreOrder(const Node& node) const;
	int Size();
	int minValue(Node& root);
	bool Contains(int value,Node& root);
	Node& GetRootNode() { return mRoot; }
	Node* CreateNode(int data);
	int GetSuccessor(int value,Node& root);
private:
	void InsertSortedNode(Node* node, Node& root);
	Node mRoot;
	int count = 0;
	int minVal = mRoot.mData;
	int successor = 0;
};