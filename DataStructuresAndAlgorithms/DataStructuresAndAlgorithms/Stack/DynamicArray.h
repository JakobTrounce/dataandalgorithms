#pragma once

template<class T> class DynamicArray
{

public:
	DynamicArray() {}
	DynamicArray(int size,int grow)
		: growby(grow),
		oldSize(size),
		size(size)
	{
		content = new T[size];
	};
	~DynamicArray() {}

	void Add(T val)
	{
			if (tracker > oldSize)
			{
				Expand();
				content[tracker] = val;
			}
			else
			{
				content[tracker] = val;
			}
		tracker++;
	}

	void Remove(int index)
	{
		DynamicArray temp(1, 2);
		for (int i = index; i < size - 1; ++i)
		{
			content[i] = content[i + 1];
		}
		content[size] = temp[0];
	}

	void Removeb(T val)
	{
		DynamicArray <T>temp(1, 1);
		bool found = false;
		for (int i = 0; i < oldSize; ++i)
		{
			if (content[i] == val)
			{
				found = true;
			}
			if (found)
			{
				if (i < oldSize - 1)
				{
					content[i] = content[i] + 1;
				}
				else
				{
					content[i] = temp[0];
				}
			}
		}
	}

	void Clear()
	{
		T* temp = new T[size];
		for (int i = 0; i < size; ++i)
		{
			content[i] = temp[i];
		}
	}

	T operator[](int i)
	{
		return content[i];
	}

	bool Contains(T val)
	{
		for (int i = 0; i < size; ++i)
		{
			if (content[i] == val)
			{
				return true;
			}

		}
		return false;
	}

	int SortedSearch(T val)
	{
		int left = 0;
		int right = size - 1;
		int current = 0;
		while (true)
		{
			current = (left + right) *0.5;
			if (content[current] == val)
			{
				return current;
			}
			else if (left > right)
			{
				return false;
			}
			else
			{
				if (content[current] < val)
				
					left = current + 1;
				else
					right = current - 1;
				
			}
		}
		return -1;
	}

	void Sort()
	{
		for (int x = 0; x < size; ++x)
		{
			for (int y = 0; y < size; ++y)
			{
				T temp;
				if (content[y] != nullptr && content[x] != nullptr)
				{
					if (content[y] > content[x])
					{
						temp = content[y];
						content[y] = content[x];
						content[x] = temp;
					}
				}
			}
		}
	}
private:
	T *content;
	int growby;
	int oldSize;
	int size;
	int tracker = 0;
	////////
	void Expand()
	{
		oldSize = size;
		size *= growby;
		T *temp = new T[size];
		for (int i = 0; i < size; ++i)
		{
			temp[i] = content[i];

		}
		content = nullptr;
		delete[] content;
		content = temp;
	}


};






