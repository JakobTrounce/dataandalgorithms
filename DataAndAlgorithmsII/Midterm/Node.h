#pragma once


/*
	add
	if > go right if < go left
	if no child then  insert
*/
struct Node
{
	int mData;
	Node* leftNode;
	Node* rightNode;
};