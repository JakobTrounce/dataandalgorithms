#ifndef INCLUDED_DATATABLE_H
#define INCLUDED_DATATABLE_H
#include <iostream>
#include <string>
#include <queue>
#include <fstream>
#include <mutex>
#include <thread>

using std::thread;


class DataTable
{
public:
	struct playerData
	{
		std::string date;
		std::string userId;
		std::string actionId;
		std::string actionParameters;
	};

	void storeData(std::string date,std::string userId,std::string actionId,std::string actionParameters);
	//void storeData();
	void WriteToDisk();

private:
	std::queue<playerData> stored;
	std::queue<playerData> toWrite;
	std::ofstream storage;
	int maxSize = 100;
	std::mutex mtx;
};

#endif
