#pragma once
#include "Node.h"
template <typename T>
class Tree
{
public:
	Tree()
	{
		{
			mRoot = Node();
			mRoot.mData = -1;
		}
	}
	Tree(T nodeValue)
	{
		mRoot = Node();
		mRoot.mData = nodeValue;
	}
	~Tree()
	{

	}

	void SortedListToTree(vector<T> nums)
	{
		int leftIndex = 0;
		int rightIndex = std::size(nums) - 1;
		mRoot = *(CreateTreeFromList(nums, leftIndex, rightIndex));
	}
	void PrintPreOrder(const Node<T>& node) const
	{
	
		//root
		std::cout << node.mData;
		//left
		if (node.leftNode != nullptr)
		{
			PrintPreOrder(*(node.leftNode));
		}
		//right
		if (node.rightNode != nullptr)
		{
			PrintPreOrder(*(node.rightNode));
		}
	
	}
	void PrintInOrder(const Node<T>& node) const
	{
		//left
		if (node.leftNode != nullptr)
		{
			PrintInOrder(*(node.leftNode));
		}
		//root
		std::cout << node.mData;
		//right
		if (node.rightNode != nullptr)
		{
			PrintInOrder(*(node.rightNode));
		}
	}
	void PrintPostOrder(const Node<T>& node) const
	{
		//left
		if (node.leftNode != nullptr)
		{
			PrintPostOrder(*(node.leftNode));
		}
		//right
		if (node.rightNode != nullptr)
		{
			PrintPostOrder(*(node.rightNode));
		}
		//Root
		std::cout << node.mData;
	}
	Node<T>& GetRootNode() { return mRoot; }
	Node<T>* CreateNode(T data)
	{
		Node<T>* pNode = new Node();
		pNode->mData = data;
		pNode->leftNode = nullptr;
		pNode->rightNode = nullptr;

		return pNode;
	}
	void InsertSorted(Node<T>* node, Node<T>& root)
	{
		//root
		if (node->mData < root.mData && root.leftNode != nullptr)
		{
			InsertSorted(node, *(root.leftNode));
		}
		if (node->mData > root.mData && root.rightNode != nullptr)
		{
			InsertSorted(node, *(root.rightNode));
		}
		else if (node->mData < root.mData)
		{
			root.leftNode = node;
		}
		else
		{
			root.rightNode = node;
		}
	}
private:
	Node<T>* CreateTreeFromList(vector<T> nums, int leftIndex, int rightIndex)
	{
		//BaseCase
		if (leftIndex >= rightIndex)
		{
			Node<T>* pNode = CreateNode(nums[leftIndex]);
			return nullptr;
		}
		// Get The Middle element, make it the root
		int mid = (leftIndex + rightIndex) / 2;
		Node* root = CreateNode(nums[mid]);
		// recursively make the left side of tree
		root->leftNode = CreateTreeFromList(nums, leftIndex, (mid));
		// recursively make right side of tree
		root->rightNode = CreateTreeFromList(nums, (mid + 1), rightIndex);
		// return the root
		return root;
	}
	Node<T> mRoot;
};