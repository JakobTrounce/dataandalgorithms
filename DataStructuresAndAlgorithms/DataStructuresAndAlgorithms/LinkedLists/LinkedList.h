#pragma once

template<typename T>
class LinkedList
{
public:
	struct Node
	{
		T data;
		Node *next;
	};

	void AddFirst(T data)
	{
		Node* travel;
		travel = head;
		if (head == nullptr)
		{
			head = new Node;
			tail = head;
			head->data = data;
		}
		else
		{
			travel = new Node;
			travel->data = data;
			T temp;
			temp = head->data;
			head->data = travel->data;
			travel->data = temp;
			travel->next = head;

		}
	}

	void AddLast(T data)
	{
		Node* travel;
		if (head == nullptr)
		{
			travel = new Node;
			travel->data = data;
			head = travel;
			tail = head;
		}
		else
		{
			travel = new Node;
			travel->next = head;
			while (travel->next != tail)
			{
				travel = travel->next;
			}
			travel = new Node;
			travel->data = tail->data;
			travel->next = tail;
			tail = new Node;
			tail->data = data;
		}


	}

	int Find(T data)
	{
		Node* travel = head;
		while (travel->data != data)
		{
			travel = travel->next;
		}
		
		return count;
	}

	void Remove(Node lost)
	{
		Node* remove = Find(lost.data);
		delete remove;
		Node* traveler = head;
		while (traveler != remove)
		{
			traveler = traveler->next;
		}
		traveler->next = remove->next;
		delete remove;
	}

	void RemoveFirst()
	{
		Node* temp = new Node;
		temp->data = head->data;
		temp->next = head->next;
		delete head;
		head = temp;

	}

	void RemoveLast()
	{
		Node*temp = new Node;
		Node* travel;
		temp->data = tail->data;
		temp->next = tail->next;
		delete tail;
		while (travel->next != nullptr)
		{
			travel = travel->next;
		}

		travel->next = temp;
		tail = temp;

	}

	void Reverse()
	{
		Node* travel = new Node;
		travel->next = head;
		while (travel->next != tail)
		{
			travel = travel->next;
		}
		tail->next = travel;
		Node* temp = tail->next;
		do
		{

			if (travel->next == temp)
			{
				temp->next = travel;
				travel = head;
			}
			 if (head->next == temp)
			{
				temp->next = head;
				temp = temp->next;
			}
		} while (temp != head);
	}
private:

	Node* head = nullptr;
	Node* tail = nullptr;
	int count = 0;
};