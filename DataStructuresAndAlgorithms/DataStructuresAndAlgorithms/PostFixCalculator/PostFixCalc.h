#pragma once
#include "ArrayStack.h"
#include <string>
class PostFixCalc
{
public:
	PostFixCalc() {}

	int EvaluateExpression(std::string exp);
	int Calculate(char operation, int num1, int num2);
	bool IsOperator(char val);
	bool IsNumeric(char val);

private:

	int size = 5;
	int grow = 2;
	ArrayStack<int> expression;
};