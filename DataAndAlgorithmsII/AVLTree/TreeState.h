enum class TreeState
{
	Balanced,
	LeftHeavy,
	RightHeavy
};